import json
import boto3
import decimal
import logging
import uuid
import re
import gzip
import os
from datetime import datetime, timedelta
from chalice import Chalice, BadRequestError
from boto3.dynamodb.conditions import Key, Attr
from googleads import dfp, oauth2, errors
import tempfile

app = Chalice(app_name='liteturn-api')
app.debug = True
dynamodb = boto3.resource('dynamodb')
s3 = boto3.client('s3')

# Google API Setup Info
appClientId = '728269833024-djfvj4rqlsgg4b29og60esl97hgc8kv5.apps.googleusercontent.com'
appClientSecret = 'xxxxx-xxxxxxxxxxx'
userRefreshToken = '' #We'll get this later from the user's stored connection
appName = 'Liteturn APP'

# AWS Config Info
authorizerId = 'gjcwzy'

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

#################
### DFP API Utils
#################
def getDFPClient(appClientId,appClientSecret,userRefreshToken,appName):
    try:
        oauth2_client = oauth2.GoogleRefreshTokenClient(appClientId, appClientSecret, userRefreshToken)
        dfp_client = dfp.DfpClient(oauth2_client, appName)
        return dfp_client
    except ValueError as e:
        raise ValueError()
def getDFPNetworks(DFPClient):
    ## This is super hacky and I'm really pressed for time. Sorry. 
    try:
        networks = {}
        networks['networks'] = []
        result = DFPClient.GetService('NetworkService').getAllNetworks()
        for network in result:
            a = {}
            a['id'] = network['id']
            a['displayName'] = network['displayName']
            a['networkCode'] = network['networkCode']
            a['propertyCode'] = network['propertyCode']
            a['timeZone'] = network['timeZone']
            a['currencyCode'] = network['currencyCode']
            networks['networks'].append(a)
        return networks
    except ValueError as e:
        raise ValueError()
def getDFPOrders(DFPClient,networkCode):
    try:
        orders = {}
        orders['orders'] = []
        result = DFPClient.GetService('NetworkService').getAllNetworks()
        DFPClient.network_code = networkCode
        queryFilter = dfp.FilterStatement()
        result = DFPClient.GetService('OrderService', version='v201702').getOrdersByStatement(queryFilter.ToStatement())
        for order in result['results']:
            a = {}
            a['id'] = order['id']
            a['name'] = order['name']
            orders['orders'].append(a)
        return orders
    except ValueError as e:
        raise ValueError()

def runDFPReport(connectionId,orderId,accountId,reportId,reportInstanceId):

    # NOTE: This is a blocking function which is a terrible thing to do while running in AWS Lambda. (Or any place really)
    # Because I'm using this client library, I don't have a better way to do this right now. We should look at queuing this in SQS or something in the future. 
    # I'm making this tradeoff with this napkin math equation in mind. (100 Reports x 10 customers) x 60 seconds for the report to run = 60,000 Lambda seconds per month. You currently get 400,000 seconds per month for free. 

    # This is pulled from the DFP examples: https://github.com/googleads/googleads-python-lib/blob/master/examples/dfp/v201702/report_service/run_delivery_report.py
 #   DFPClient.network_code = networkCode
 #   values = [{
 #   'key': 'id',
 #   'value': {
 #       'xsi_type': 'NumberValue',
 #       'value': orderId
 #   }
 #   }]
 #   filter_statement = {'query': 'WHERE ORDER_ID = :id','values': values}
 #   end_date = datetime.now()
 #   start_date = end_date - timedelta(days=60)
 #   report_job = {
 #       'reportQuery': {
 #           'dimensions': ['ORDER_ID', 'ORDER_NAME'],
 #           'dimensionAttributes': ['ORDER_TRAFFICKER', 'ORDER_START_DATE_TIME', 'ORDER_END_DATE_TIME'],
 #           'statement': filter_statement,
 #           'columns': ['AD_SERVER_IMPRESSIONS', 'AD_SERVER_CLICKS',
 #                   'AD_SERVER_CTR', 'AD_SERVER_CPM_AND_CPC_REVENUE',
 #                   'AD_SERVER_WITHOUT_CPD_AVERAGE_ECPM'],
 #           'dateRangeType': 'CUSTOM_DATE',
 #           'startDate': {'year': start_date.year,'month': start_date.month,'day': start_date.day},
 #           'endDate': {'year': end_date.year,'month': end_date.month,'day': end_date.day}
 #       }
 #   }
 #   # Initialize a DataDownloader.
 #   report_downloader = DFPClient.GetDataDownloader(version='v201702')
#
 #   try:
 #       # Run the report and wait for it to finish.
 #       report_job_id = report_downloader.WaitForReport(report_job)
 #   except errors.DfpReportError, e:
 #       print 'Failed to generate report. Error was: %s' % e
#
 #   export_format = 'CSV_DUMP'
 #   filename = '/tmp/'+reportInstanceId+".csv.gz"
 #   report_file = open(filename, "w+")
 #   #Download report data.
 #   report_downloader.DownloadReportToFile(report_job_id, export_format, report_file)
 #   report_file.close()#
 #   # Set the bucket info and upload the data file to s3
 #   s3location = accountId+"/"+reportId+"/"+reportInstanceId+".csv.gz"
 #   s3bucket = "liteturn-raw"
 #   try:
 #       s3.upload_file(Filename=filename, Bucket=s3bucket, Key=s3location)
 #       # Display results.
 #       print 'File uploaded to S3 at: %s/%s' % (s3bucket,s3location)
 #       os.remove(filename)
 #       return(True)
 #   except Exception as e:
 #       print(e)
 #       print('Error putting object {} to bucket {}.'.format(key, bucket))
 #       raise e
    message = {
        "connectionId": connectionId,
        "orderId":orderId,
        "accountId":accountId,
        "reportId":reportId,
        "reportInstanceId": reportInstanceId
    }
    client = boto3.client('sns')
    response = client.publish(
        TargetArn="arn:aws:sns:us-east-1:020657569316:getData",
        Message=json.dumps({'default': json.dumps(message)}),
        MessageStructure='json'
    )

### Users
def getUsers():
    try:
        table = dynamodb.Table('Users')
        response = table.scan()
    except ValueError as e:
        print(e.response['Error']['Message'])

def createUser():
    #Export either a user object or error
    a="a"
###############
### Connections
############### 
def getConnectionsByAccount(account_id, connection_id=None, provider=None):
    # This gets either all of the connections for an account or a specific connection if requested.
    connections = {}
    if connection_id != None:
        try:
            table = dynamodb.Table('Connections')
            response = table.query(
                ProjectionExpression="#act, #n, #s, #i, #con, #p, #rt, #nn, #nc",
                ExpressionAttributeNames={ "#act": "accountId", "#n": "name", "#s": "status", "#i":"image", "#con": "connectionId", "#p": "provider", "#rt": "refreshToken", "#nn":"networkName", "#nc":"networkCode"},
                KeyConditionExpression=Key("accountId").eq(account_id) & Key("connectionId").eq(connection_id)
            )
            connections = {"connections": response['Items']}
            return connections
        except ValueError as e:
            print(e.response['Error']['Message'])
            return {'response': 'Error getting connections'}
    if provider != None:
        try:
            table = dynamodb.Table('Connections')
            response = table.query(
                ProjectionExpression="#act, #n, #s, #i, #con, #p, #rt, #nn, #nc",
                ExpressionAttributeNames={ "#act": "accountId", "#n": "name", "#s": "status", "#i":"image", "#con": "connectionId", "#p": "provider", "#rt": "refreshToken", "#nn":"networkName", "#nc":"networkCode"},
                FilterExpression="#p = :p",
                ExpressionAttributeValues={":p":provider},
                KeyConditionExpression=Key("accountId").eq(account_id)

            )
            connections = {"connections": response['Items']}
            return connections
        except ValueError as e:
            print(e.response['Error']['Message'])
            return {'response': 'Error getting connections'}    
    else:
        try:
            table = dynamodb.Table('Connections')
            response = table.query(
                ProjectionExpression="#act, #n, #s, #i, #con, #p, #rt, #nn, #nc",
                ExpressionAttributeNames={ "#act": "accountId", "#n": "name", "#s": "status", "#i":"image", "#con": "connectionId", "#p": "provider", "#rt": "refreshToken", "#nn":"networkName", "#nc":"networkCode"},
                KeyConditionExpression=Key("accountId").eq(account_id)
            )
            connections = {"connections": response['Items']}
            return connections
        except ValueError as e:
            print(e.response['Error']['Message'])
            return {'response': 'Error getting connections'}

def createConnection(account_id, refresh_token, provider):
    # This creates a new connection in the database. 
    # Right now, there is only the provider of DFP. As we add more connection providers,
    # This should be pulled into a map somewhere.
    # Also, How to get the network ID into the connection is still something that needs to be added
    
    connection_id = str(uuid.uuid4()) # Create a UUID for the new connection being created.
    if provider == "DFP":
        image = "images/DFP-Logo1.jpg" # In the future, This should be pulled from a mapping somewhere
        name = "DFP Connection"
        provider = "DFP"
        status = "Active"
    elif provider == "GA":
        image = "images/GA-Logo1.png" # In the future, This should be pulled from a mapping somewhere
        name = "Google Analytics Connection"
        provider = "GA"
        status = "Active"
    else:
        image = "Unknown" # In the future, This should be pulled from a mapping somewhere
        name = "Unknown"
        provider = "Unknown"
        status = "Unknown"
    try:
        table = dynamodb.Table('Connections')
        response = table.put_item(
           Item={
                'accountId': account_id,
                'connectionId': connection_id,
                'image': image,
                'name': name,
                #'networkCode': '', #I need to get this and update the record. 
                'provider': provider,
                'refreshToken': refresh_token,
                'status': status
            }
        )
        connection = {"response": "Connection created successfully"}
        return connection
    except ValueError as e:
        print(e.response['Error']['Message'])
        return {'response': 'Error creating connection'}   

def updateConnection(account_id,connection_id,data):
    allowedFields = ["connectionName","networkName","networkCode"]
    # An attempt to clean
    rx = re.compile('\W+')
    connectionName = rx.sub(' ', data['connectionName']).strip()
    networkName = rx.sub(' ', data['networkName']).strip()
    networkCode = rx.sub(' ', data['networkCode']).strip()
    print(networkName)
    try:
        table = dynamodb.Table('Connections')
        response = table.update_item(
            Key={
                'accountId': account_id,
                'connectionId': connection_id
            },
            UpdateExpression="set #n = :cn, #nc = :nc, #nn =:nn",
            ExpressionAttributeValues={
                ':cn': connectionName,
                ':nc': networkCode,
                ':nn': networkName
            },
            ExpressionAttributeNames={"#n": "name", "#nc": "networkCode", "#nn":"networkName"},
            ReturnValues="UPDATED_NEW"
        )
        connection = {"response": "Values updated successfully"}
        return connection
    except ValueError as e:
        print(e.response['Error']['Message'])
        return {'response': 'Error creating connection'}   



###########
### Reports
###########
def getReports(account_id=None):
    # This will return a list of the reports that are available to run. 
    # There will be both public and private (custom) reports that will be available
    # This should query for a list of reports that are public (all customers have access)
    # If an account is passed in, it should show both the public reports as well as the reports that are private for that client.
    reports = {}
    if account_id != None:
        try:
            table = dynamodb.Table('Reports')
            response = table.scan(
                ProjectionExpression="#rid, #n, #s, #i, #cre, #d, #cr",
                ExpressionAttributeNames={ "#rid": "reportId", "#n": "name", "#s": "status", "#i":"image", "#cre": "created", "#pid": "privateAccountId", "#d": "description", "#cr":"customRibbon" },
                FilterExpression="#s = :s or #pid = :pid ",
                ExpressionAttributeValues={":s":"public",":pid":account_id}
            )
            reports = {"reports": response['Items']}
            return reports
        except ValueError as e:
            print(e.response['Error']['Message'])
            return {'response': 'Error getting connections'}
    else:
        try:
            table = dynamodb.Table('Reports')
            response = table.scan(
                ProjectionExpression="#rid, #n, #s, #i, #cre, #d, #cr",
                ExpressionAttributeNames={ "#rid": "reportId", "#n": "name", "#s": "status", "#i":"image", "#cre": "created", "#d": "description", "#cr":"customRibbon" },
                FilterExpression="#s = :s",
                ExpressionAttributeValues={":s":"public"}
            )
            reports = {"reports": response['Items']}
            return reports
        except ValueError as e:
            print(e.response['Error']['Message'])
            return {'response': 'Error getting connections'}  

def getReport(report_id,account_id=None):
    report = {}
    if account_id != None:
        try:
            table = dynamodb.Table('Reports')
            response = table.query(
                ProjectionExpression="#rid, #n, #s, #i, #cre, #d, #ct",
                ExpressionAttributeNames={ "#rid": "reportId", "#n": "name", "#s": "status", "#i":"image", "#cre": "created", "#pid": "privateAccountId", "#d": "description", "#ct": "connectionType" },
                KeyConditionExpression=Key('reportId').eq(report_id),
                FilterExpression="#s = :s or #pid = :aid ",
                ExpressionAttributeValues={":s":"public",":aid":account_id}
            )
            reports = {"reports": response['Items']}
            return reports
        except ValueError as e:
            print(e.response['Error']['Message'])
            return {'response': 'Error getting connections'}
    else:
        try:
            table = dynamodb.Table('Reports')
            response = table.query(
                ProjectionExpression="#rid, #n, #s, #i, #cre, #d, #ct",
                ExpressionAttributeNames={ "#rid": "reportId", "#n": "name", "#s": "status", "#i":"image", "#cre": "created", "#d": "description", "#ct": "connectionType" },
                KeyConditionExpression=Key('reportId').eq(report_id),
                FilterExpression="#s = :s",
                ExpressionAttributeValues={":s":"public"}
            )
            reports = {"reports": response['Items']}
            return reports
        except ValueError as e:
            print(e.response['Error']['Message'])
            return {'response': 'Error getting connections'}    

def createReportInstance(account_id,report_data):
    # This creates a new report instance in the database. 
    # A Report Instance is an individual report run. The details of each report that is run should be stored here.
    # We will have 3 states (aka statuses) Running, Calculating, and Completed. Running - kicked of the report. Calculating - When it's being processed. Completed - When it's done.

    #Get the connection that will be used to run the report. You need these because it contains the refresh token and network code for creating the DFP Client to make requests
    connection = getConnectionsByAccount(account_id,connection_id=report_data['connectionId'])
    userRefreshToken = connection['connections'][0]['refreshToken']
    networkCode = connection['connections'][0]['networkCode']
    DFPClient = getDFPClient(appClientId,appClientSecret,userRefreshToken,appName) #appClientId and appClientSecret are global vars
    
    
    reportInstanceId = str(uuid.uuid4()) # Create a UUID for the new report instance
    #Now that I have the DFP Client, I can request a report

    runDFPReport(report_data['connectionId'],report_data['orderId'],account_id,report_data['reportId'],reportInstanceId)

    connection = getConnectionsByAccount(account_id,report_data['connectionId'])
    networkCode = connection['connections'][0]['networkCode']
    createdAt = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    status = "Running" 
    if report_data['reportId'] == "1": #I don't know if there is special shit I need to do with each individual report. For now doing this but likely rip this out.
        connectionId = report_data['connectionId']
        connectionName = connection['connections'][0]['name']
        reportId =  report_data['reportId']
        reportName =  report_data['reportName']
        orderId =  report_data['orderId']
        orderName = report_data['orderName']
        createdById = report_data['createdById']
        createdByName = report_data['createdByName']
    else:
        connectionId = report_data['connectionId']
        connectionName = connection['connections'][0]['name']
        reportId =  report_data['reportId']
        reportName =  report_data['reportName']
        orderId =  report_data['orderId']
        orderName = report_data['orderName']
        createdById = report_data['createdById']
        createdByName = report_data['createdByName']
    try:
        table = dynamodb.Table('ReportHistory')
        response = table.put_item(
           Item={
                'accountId': account_id,
                'reportInstanceId': reportInstanceId,
                'connectionId': connectionId,
                'connectionName': connectionName,
                'reportId': reportId,
                'reportName': reportName,
                'orderId': orderId,
                'orderName': orderName,
                'createdById': createdById,
                'createdByName': createdByName,
                'date': createdAt,
                'status': status,
                'networkCode': networkCode
            }
        )
        connection = {"response": "report instance created successfully", "reportInstanceId":reportInstanceId }
        return connection
    except ValueError as e:
        print(e.response['Error']['Message'])
        return {'response': 'Error creating report instance'} 

def getReportHistory(account_id):
    # This gets all of the reports that have been run by a particular account. 
    reportHistory = {}
    try:
        table = dynamodb.Table('ReportHistory')
        response = table.query(
            ProjectionExpression="#act, #rn, #rid, #s, #cid, #cn, #oid, #on, #cbi, #cbn,#d,#riid",
            ExpressionAttributeNames={ "#act": "accountId", "#rn": "reportName","#rid": "reportId", "#s": "status", "#cid": "connectionId","#cn": "connectionName", "#oid": "orderId","#on": "orderName", "#cbi": "createdById","#cbn": "createdByName", "#d": "date", "#riid": "reportInstanceId"},
            KeyConditionExpression=Key("accountId").eq(account_id),
            ScanIndexForward=False
        )
        reportHistory = {"reportInstances": response['Items']}
        return reportHistory
    except ValueError as e:
        print(e.response['Error']['Message'])
        return {'response': 'Error getting report history'}

def getReportInstance(account_id,report_instance_id):
    reportInstance = {}
    try:
        table = dynamodb.Table('ReportHistory')
        response = table.query(
            ProjectionExpression="#act, #rn, #rid, #s, #cid, #cn, #oid, #on, #cbi, #cbn,#d,#riid",
            ExpressionAttributeNames={ "#act": "accountId", "#rn": "reportName","#rid": "reportId", "#s": "status", "#cid": "connectionId","#cn": "connectionName", "#oid": "orderId","#on": "orderName", "#cbi": "createdById","#cbn": "createdByName", "#d": "date", "#riid": "reportInstanceId"},
            KeyConditionExpression=Key("accountId").eq(account_id),
            FilterExpression="#riid = :riid",
            ExpressionAttributeValues={":riid":report_instance_id}
        )
        reportInstance = {"reportInstances": response['Items']}
        return reportInstance
    except ValueError as e:
        print(e.response['Error']['Message'])
        return {'response': 'Error getting connections'}


#######################
### Application Router
#######################

@app.route('/', authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def index():
    return {'liteturn': 'api'}

@app.route('/users', methods=['GET'], authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def users():
    return getUsers()

@app.route('/accounts', authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def accounts():
    return {'hello': 'world'}

@app.route('/connections', methods=['POST','GET'], authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def connections():
    request = app.current_request
    if request.method == 'POST':
        if "accountId" in request.json_body and "refreshToken" in request.json_body and "provider" in request.json_body:
            response = createConnection(request.json_body["accountId"],request.json_body["refreshToken"],request.json_body["provider"])
            return response
        else:
            raise BadRequestError("accountId, refreshToken, provider not provided.")
    elif request.method == 'GET' and "accountId" in request.query_params:
        if "provider" in request.query_params:
            return getConnectionsByAccount(account_id=request.query_params["accountId"],provider=request.query_params["provider"])
        else:
            return getConnectionsByAccount(request.query_params["accountId"])
    else:
        return {}

@app.route('/connections/{connection_id}', methods=['PATCH','GET'], authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def connectionsConnection(connection_id):
    request = app.current_request
    if request.method == 'GET':
        if "accountId" in request.query_params:
            return getConnectionsByAccount(request.query_params["accountId"],connection_id=connection_id)
        else:
            return {}
    elif request.method == 'PATCH':
        if "accountId" in request.query_params:
            if "connectionName" in request.json_body or "networkCode" in request.json_body or "networkName" in request.json_body:
                return updateConnection(account_id=request.query_params["accountId"],connection_id=connection_id,data=request.json_body)
            else:
                return {}
        else:
            return {}
    else:
        return {}

@app.route('/connections/{connection_id}/{option}', authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def connectionsConnectionOption(connection_id,option):
    request = app.current_request
    if "accountId" in request.query_params:
        connection = getConnectionsByAccount(request.query_params["accountId"],connection_id=connection_id)        
        if option == "networks":
            userRefreshToken = connection['connections'][0]['refreshToken']
            DFPClient = getDFPClient(appClientId,appClientSecret,userRefreshToken,appName)
            return getDFPNetworks(DFPClient)
        elif option == "orders":
            userRefreshToken = connection['connections'][0]['refreshToken']
            connectionNetwork = connection['connections'][0]['networkCode']
            DFPClient = getDFPClient(appClientId,appClientSecret,userRefreshToken,appName)
            return getDFPOrders(DFPClient,connectionNetwork)
        else:
            return {}
    else:
        return {}

@app.route('/reports',cors=True)
def reports():
    request = app.current_request
    if "accountId" in request.query_params:
        return getReports(account_id=request.query_params["accountId"])
    else:
        return getReports()

@app.route('/reports/{report_id}', authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def reportsReport(report_id):
    request = app.current_request
    if "accountId" in request.query_params:
        report = getReport(report_id=report_id,account_id=request.query_params["accountId"])
        return report
    else:
        return {}

@app.route('/report-history', methods=['POST','GET'], authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def reportHistory():
    request = app.current_request
    if request.method == 'POST' and "accountId" in request.query_params:
        if "connectionId" in request.json_body and "reportId" in request.json_body:
            response = createReportInstance(request.query_params["accountId"],request.json_body)
            return response
        else:
            raise BadRequestError("connectionId, reportId not provided.")

    elif request.method == 'GET' and "accountId" in request.query_params:
        return getReportHistory(request.query_params["accountId"])
    else:
        raise BadRequestError("accountId not provided.")

@app.route('/report-history/{report_instance_id}', authorization_type='CUSTOM', authorizer_id=authorizerId, cors=True)
def reportInstance(report_instance_id):
    request = app.current_request
    if "accountId" in request.query_params:
        return getReportInstance(request.query_params["accountId"],report_instance_id=report_instance_id)
    else:
        raise BadRequestError("accountId not provided.")

@app.route('/healthcheck',cors=True)
def getHealth():
    return {'api': 'ok'}

