# liteturn-api

## Installation

* `git clone https://github.com/<this-repository>`
* `cd liteturn-processor`
* `pip install virtualenv`
* `virtualenv ./venv`
* `source venv/bin/activate`
* `pip install chalice boto3 googleads`

## Credentials

Before you can deploy an application, be sure you have
credentials configured.  If you have previously configured your
machine to run boto3 (the AWS SDK for Python) or the AWS CLI then
you can skip this section.

If this is your first time configuring credentials for AWS you
can follow these steps to quickly get started::

    $ mkdir ~/.aws
    $ cat >> ~/.aws/config
    [default]
    aws_access_key_id=YOUR_ACCESS_KEY_HERE
    aws_secret_access_key=YOUR_SECRET_ACCESS_KEY
    region=YOUR_REGION (us-east-1)

## Running / Development

* `chalice local`

## Deploying to AWS Lambda

* `chalice deploy`